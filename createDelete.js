const fs = require('fs');
const { promisify } = require('util');
const writeFileAsync = promisify(fs.writeFile);
const unlinkAsync = promisify(fs.unlink);

Promise.all([
    writeFileAsync('file1.txt', 'Hello World!'),
    writeFileAsync('file2.txt', 'Goodbye World!')
])
    .then(() => {
        console.log("Both Files Created");

        setTimeout(() => {

            unlinkAsync('file1.txt')
                .then(console.log('file1.txt deleted'))
                .then(() => unlinkAsync('file2.txt'))
                .then(console.log('file2.txt deleted'))
                .catch((err) => console.log(err));
        }, 2000);
    })
    .catch((err) => console.error(err));