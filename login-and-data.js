const fs = require('fs');
const { promisify } = require('util');
const append = promisify(fs.appendFile);

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    const now = new Date();
    const logEntry = `${now.toISOString()} - User ${user.id}: ${activity}\n`;
    append('log.txt', logEntry);
}

const user = {id: 2, name: 'Shubham'};

login(user, 2)
    .then(() => {
        logData(user, "Login Success");
        return getData();
    })
    .then((data) => {
        logData(user, "GetData Success");
    })
    .catch((err) => {
        if (err.message === "User not found") {
            logData(user, "Login Failure");
        } else {
            logData(user, "GetData Failure");
        }
    });


/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"
 
    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/