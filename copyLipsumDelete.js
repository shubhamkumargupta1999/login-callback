const fs = require('fs');
const { promisify } = require('util');
const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const unlinkAsync = promisify(fs.unlink);

readFileAsync('./lipsum.txt')
    .then((data) => writeFileAsync('./copy.txt', data))
    .then(() => console.log("Lipsum File Copied"))
    .then(() => unlinkAsync('./lipsum.txt'))
    .then(() => console.log("Lipsum file deleted."))
    .catch((err) => console.log(err))